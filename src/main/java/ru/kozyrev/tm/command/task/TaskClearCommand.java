package ru.kozyrev.tm.command.task;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.TerminalUtil;

public final class TaskClearCommand extends AbstractCommand {

    @Override
    public final String getName() {
        return "task-clear";
    }

    @Override
    public final String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[CLEAR]");

        System.out.println("Press 'ENTER' FOR ALL OR PROJECT ID]");
        String projectNum = terminalService.nextLine();

        if (projectNum.isEmpty()) {
            TerminalUtil.clearAllTasks(serviceLocator.getTaskService(), terminalService.getCurrentUserId());
        } else {
            String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, terminalService.getCurrentUserId());
            TerminalUtil.clearTasks(serviceLocator.getTaskService(), projectId, terminalService.getCurrentUserId());
        }
    }
}
