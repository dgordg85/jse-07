package ru.kozyrev.tm.command.task;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.entity.EntityException;
import ru.kozyrev.tm.util.DateUtil;

public final class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand() {
        this.secure = false;
    }

    @Override
    public final String getName() {
        return "task-create";
    }

    @Override
    public final String getDescription() {
        return "Create new tasks.";
    }

    @Override
    public final void execute() throws Exception {
        Task task = new Task();

        System.out.println("[TASK CREATE]\nENTER NAME:");
        task.setName(terminalService.nextLine());

        System.out.println("ENTER PROJECT ID:");
        task.setProjectId(serviceLocator.getProjectService().getEntityIdByNum(terminalService.nextLine(), terminalService.getCurrentUserId()));

        System.out.println("ENTER DESCRIPTION:");
        task.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATESTART:");
        task.setDateStart(DateUtil.parseDate(terminalService.nextLine()));

        System.out.println("ENTER DATEFINISH:");

        task.setDateFinish(DateUtil.parseDate(terminalService.nextLine()));

        task.setUserId(terminalService.getCurrentUser().getId());

        if (serviceLocator.getTaskService().persist(task, terminalService.getCurrentUserId()) == null) {
            throw new EntityException();
        }
        System.out.println("[OK]");
    }
}
