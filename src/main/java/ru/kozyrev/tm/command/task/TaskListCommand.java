package ru.kozyrev.tm.command.task;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @Override
    public final String getName() {
        return "task-list";
    }

    @Override
    public final String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("PRESS 'ENTER' FOR ALL OR INPUT ID PROJECT");
        String projectNum = terminalService.nextLine();

        if (projectNum.isEmpty()) {
            TerminalUtil.printAllTasks(serviceLocator.getTaskService().findAll(terminalService.getCurrentUserId()));
        } else {
            String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, terminalService.getCurrentUserId());
            List<Task> taskList = serviceLocator.getTaskService().findAll(projectId, terminalService.getCurrentUserId());
            TerminalUtil.printTasksOfProject(taskList);
        }
    }
}
