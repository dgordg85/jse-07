package ru.kozyrev.tm.command.task;

import ru.kozyrev.tm.command.AbstractCommand;

public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public final String getName() {
        return "task-remove";
    }

    @Override
    public final String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[TASK DELETE]\nENTER ID:");
        String taskNum = terminalService.nextLine();

        String taskId = serviceLocator.getTaskService().getEntityIdByNum(taskNum, terminalService.getCurrentUserId());
        serviceLocator.getTaskService().remove(taskId, terminalService.getCurrentUserId());
        System.out.println("[OK]");
    }
}
