package ru.kozyrev.tm.command.task;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.entity.EntityException;
import ru.kozyrev.tm.util.DateUtil;

public final class TaskUpdateCommand extends AbstractCommand {

    @Override
    public final String getName() {
        return "task-update";
    }

    @Override
    public final String getDescription() {
        return "Update selected task.";
    }

    @Override
    public final void execute() throws Exception {
        Task task = new Task();

        System.out.println("[UPDATE TASK]\nENTER ID:");
        task.setId(serviceLocator.getTaskService().getEntityIdByNum(terminalService.nextLine(), terminalService.getCurrentUserId()));

        System.out.println("NEW NAME:");
        task.setName(terminalService.nextLine());

        System.out.println("NEW PROJECT ID:");
        String projectNum = terminalService.nextLine();
        if (!projectNum.isEmpty()) {
            String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, terminalService.getCurrentUserId());
            task.setProjectId(projectId);
        } else {
            task.setProjectId("");
        }

        System.out.println("ENTER DESCRIPTION:");
        task.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATESTART:");
        String dateBegin = terminalService.nextLine();
        if (!dateBegin.isEmpty()) {
            task.setDateStart(DateUtil.parseDate(dateBegin));
        } else {
            task.setDateStart(null);
        }

        System.out.println("ENTER DATEFINISH:");
        String dateFinish = terminalService.nextLine();
        if (!dateFinish.isEmpty()) {
            task.setDateFinish(DateUtil.parseDate(dateFinish));
        } else {
            task.setDateFinish(null);
        }

        task.setUserId(terminalService.getCurrentUser().getId());

        if (serviceLocator.getTaskService().merge(task, terminalService.getCurrentUserId()) == null) {
            throw new EntityException();
        }
        System.out.println("[OK]");
    }
}
