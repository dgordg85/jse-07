package ru.kozyrev.tm.command.project;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    public final String getName() {
        return "project-list";
    }

    @Override
    public final String getDescription() {
        return "Show all projects.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("Press 'ENTER' for printing all projects list or input 'ID PROJECT'");
        String projectNum = terminalService.nextLine();

        if (projectNum.isEmpty()) {
            List<Project> projectList = serviceLocator.getProjectService().findAll(terminalService.getCurrentUserId());
            TerminalUtil.printProjects(projectList);
        } else {
            String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, terminalService.getCurrentUserId());
            List<Task> taskList = serviceLocator.getTaskService().findAll(projectId, terminalService.getCurrentUserId());
            TerminalUtil.printTasksOfProject(taskList);
        }
    }
}
