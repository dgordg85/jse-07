package ru.kozyrev.tm.command.project;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.exception.entity.EntityException;
import ru.kozyrev.tm.util.DateUtil;

public final class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public final String getName() {
        return "project-update";
    }

    @Override
    public final String getDescription() {
        return "Update Selected project.";
    }

    @Override
    public final void execute() throws Exception {
        Project project = new Project();

        System.out.println("[UPDATE PROJECT]\nENTER ID:");
        project.setId(serviceLocator.getProjectService().getEntityIdByNum(terminalService.nextLine(), terminalService.getCurrentUserId()));

        System.out.println("ENTER NAME:");
        project.setName(terminalService.nextLine());

        System.out.println("ENTER DESCRIPTION:");
        project.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATESTART:");
        project.setDateStart(DateUtil.parseDate(terminalService.nextLine()));

        System.out.println("ENTER DATEFINISH:");

        project.setDateFinish(DateUtil.parseDate(terminalService.nextLine()));

        project.setUserId(terminalService.getCurrentUser().getId());

        if (serviceLocator.getProjectService().merge(project, terminalService.getCurrentUserId()) == null) {
            throw new EntityException();
        }
        System.out.println("[OK]");
    }
}
