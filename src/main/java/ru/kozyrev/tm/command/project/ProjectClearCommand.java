package ru.kozyrev.tm.command.project;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.TerminalUtil;

public final class ProjectClearCommand extends AbstractCommand {

    @Override
    public final String getName() {
        return "project-clear";
    }

    @Override
    public final String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[CLEAR]");

        System.out.println("Press 'ENTER' FOR ALL OR PROJECT ID]");
        String projectNum = terminalService.nextLine();

        if (projectNum.isEmpty()) {
            clearAll();
        } else {
            String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, terminalService.getCurrentUserId());
            TerminalUtil.clearProject(
                    serviceLocator.getProjectService(),
                    serviceLocator.getTaskService(),
                    projectId,
                    terminalService.getCurrentUserId()
            );
        }
    }

    public final void clearAll() throws Exception {
        TerminalUtil.clearAllTasks(serviceLocator.getTaskService(), terminalService.getCurrentUserId());
        serviceLocator.getProjectService().removeAll(terminalService.getCurrentUserId());
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
