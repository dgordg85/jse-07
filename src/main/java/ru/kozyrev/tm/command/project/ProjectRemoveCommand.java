package ru.kozyrev.tm.command.project;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.TerminalUtil;

public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public final String getName() {
        return "project-remove";
    }

    @Override
    public final String getDescription() {
        return "Remove Selected project.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]\nENTER ID:");
        String projectNum = terminalService.nextLine();
        String projectId = serviceLocator.getProjectService().getEntityIdByNum(projectNum, terminalService.getCurrentUserId());
        TerminalUtil.clearProject(
                serviceLocator.getProjectService(),
                serviceLocator.getTaskService(),
                projectId,
                terminalService.getCurrentUserId()
        );
    }
}
