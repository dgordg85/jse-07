package ru.kozyrev.tm.command.project;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.util.DateUtil;
import ru.kozyrev.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public final String getName() {
        return "project-create";
    }

    @Override
    public final String getDescription() {
        return "Create new project.";
    }

    @Override
    public final void execute() throws Exception {
        Project project = new Project();

        System.out.println("[PROJECT CREATE]\nENTER NAME:");
        project.setName(terminalService.nextLine());

        System.out.println("ENTER DESCRIPTION:");
        project.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATESTART:");
        project.setDateStart(DateUtil.parseDate(terminalService.nextLine()));

        System.out.println("ENTER DATEFINISH:");
        project.setDateFinish(DateUtil.parseDate(terminalService.nextLine()));

        project.setUserId(terminalService.getCurrentUser().getId());

        serviceLocator.getProjectService().persist(project, terminalService.getCurrentUserId());

        List<Project> projectList = serviceLocator.getProjectService().findAll(terminalService.getCurrentUserId());
        TerminalUtil.printProjects(projectList);
    }
}
