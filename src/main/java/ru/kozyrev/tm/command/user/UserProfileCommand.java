package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;

public final class UserProfileCommand extends AbstractCommand {
    @Override
    public final String getName() {
        return "user-profile";
    }

    @Override
    public final String getDescription() {
        return "Show user profile";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[USER PROFILE]");
        System.out.printf("Login: %s\n", terminalService.getCurrentUser().getLogin());
        System.out.printf("Role: %s\n", terminalService.getCurrentUser().getRoleType().getDisplayName());
    }
}
