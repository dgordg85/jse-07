package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.user.UserPasswordChangeException;
import ru.kozyrev.tm.exception.user.UserPasswordMatchException;
import ru.kozyrev.tm.exception.user.UserPasswordWrongException;
import ru.kozyrev.tm.util.HashUtil;

public final class UserPasswordUpdateCommand extends AbstractCommand {
    @Override
    public final String getName() {
        return "user-pass-update";
    }

    @Override
    public final String getDescription() {
        return "Use for updating password";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[UPDATE PASSWORD]");

        System.out.println("ENTER CURRENT PASSWORD:");
        String password = terminalService.nextLine();

        String userId = terminalService.getCurrentUser().getId();
        if (serviceLocator.getUserService().isPasswordNotTrue(userId, password)) {
            throw new UserPasswordWrongException();
        }

        System.out.println("ENTER NEW PASSWORD:");
        String newPassword = terminalService.nextLine();

        System.out.println("RE-ENTER PASSWORD:");
        String reNewPassword = terminalService.nextLine();

        if (!newPassword.equals(reNewPassword)) {
            throw new UserPasswordMatchException();
        }
        User user = new User();
        user.setId(terminalService.getCurrentUser().getId());
        user.setLogin(terminalService.getCurrentUser().getLogin());
        user.setPasswordHash(HashUtil.getHash(reNewPassword));
        user.setRoleType(terminalService.getCurrentUser().getRoleType());
        user = serviceLocator.getUserService().merge(user);
        if (user == null) {
            throw new UserPasswordChangeException();
        }
        terminalService.setCurrentUser(user);
        System.out.println("[PASSWORD UPDATE]");
    }
}
