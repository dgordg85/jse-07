package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {
    @Override
    public final String getName() {
        return "user-logout";
    }

    @Override
    public final String getDescription() {
        return "Use for logout.";
    }

    @Override
    public final void execute() throws Exception {
        terminalService.setCurrentUser(null);
        System.out.println("[User logout!]");
    }
}
