package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.entity.EntityException;
import ru.kozyrev.tm.exception.user.UserPasswordMatchException;
import ru.kozyrev.tm.util.HashUtil;

public final class UserCreateCommand extends AbstractCommand {
    public UserCreateCommand() {
        secure = true;
    }

    @Override
    public final String getName() {
        return "user-create";
    }

    @Override
    public final String getDescription() {
        return "User for registry new user";
    }

    @Override
    public final void execute() throws Exception {
        User user = new User();

        System.out.println("[USER REGISTRATION]");

        System.out.println("ENTER LOGIN:");
        user.setLogin(terminalService.nextLine());

        System.out.println("ENTER PASSWORD:");
        String password = terminalService.nextLine();

        System.out.println("RE-ENTER PASSWORD:");
        String password2 = terminalService.nextLine();

        if (!password.equals(password2)) {
            throw new UserPasswordMatchException();
        }
        user.setPasswordHash(HashUtil.getHash(password));
        if (serviceLocator.getUserService().persist(user) == null) {
            throw new EntityException();
        }
        System.out.println("[OK]");
    }
}
