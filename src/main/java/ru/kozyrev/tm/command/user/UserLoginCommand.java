package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.user.UserLoginNotRegistryException;
import ru.kozyrev.tm.exception.user.UserPasswordWrongException;

public final class UserLoginCommand extends AbstractCommand {
    public UserLoginCommand() {
        secure = true;
    }

    @Override
    public final String getName() {
        return "user-login";
    }

    @Override
    public final String getDescription() {
        return "Use for logging in system.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOGIN]");

        System.out.println("ENTER LOGIN:");
        String login = terminalService.nextLine();

        User user = serviceLocator.getUserService().getUserByLogin(login);
        if (user == null) {
            throw new UserLoginNotRegistryException();
        }
        System.out.println("ENTER PASSWORD:");
        String password = terminalService.nextLine();

        if (serviceLocator.getUserService().isPasswordNotTrue(user.getId(), password)) {
            throw new UserPasswordWrongException();
        }
        terminalService.setCurrentUser(user);
        System.out.println("[User just login!]");
    }
}
