package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;

public final class UserRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public final String getDescription() {
        return "Use for deleting profile";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[USER REMOVE]");
        System.out.println("Are you sure? Type 'yes' or another for cancel...");
        String answer = terminalService.nextLine();
        if (answer.toLowerCase().equals("yes")) {
            String userId = terminalService.getCurrentUser().getId();
            serviceLocator.getUserService().remove(userId);
            System.out.println("[USER DELETE!]");
            terminalService.setCurrentUser(null);
        } else {
            System.out.println("[OPERATION ABORT!]");
        }
    }
}
