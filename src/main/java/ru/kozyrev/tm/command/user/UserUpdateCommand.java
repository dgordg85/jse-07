package ru.kozyrev.tm.command.user;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.user.UserLoginChangeFailException;
import ru.kozyrev.tm.exception.user.UserLoginEmptyException;
import ru.kozyrev.tm.exception.user.UserLoginTakenException;

public final class UserUpdateCommand extends AbstractCommand {
    @Override
    public final String getName() {
        return "user-update";
    }

    @Override
    public final String getDescription() {
        return "Update user login.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[UPDATE USER]");

        System.out.println("ENTER NEW LOGIN:");
        String login = terminalService.nextLine();
        if (login.isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (serviceLocator.getUserService().getUserByLogin(login) != null) {
            throw new UserLoginTakenException();
        }
        User user = new User();
        user.setId(terminalService.getCurrentUser().getId());
        user.setLogin(login);
        user.setPasswordHash(terminalService.getCurrentUser().getPasswordHash());
        user.setRoleType(terminalService.getCurrentUser().getRoleType());
        user = serviceLocator.getUserService().merge(user);
        if (user == null) {
            throw new UserLoginChangeFailException();
        }
        terminalService.setCurrentUser(user);
        System.out.println("[LOGIN UPDATE]");
    }
}
