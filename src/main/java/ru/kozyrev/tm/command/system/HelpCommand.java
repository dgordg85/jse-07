package ru.kozyrev.tm.command.system;

import ru.kozyrev.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    public HelpCommand() {
        secure = true;
    }

    @Override
    public final String getName() {
        return "help";
    }

    @Override
    public final String getDescription() {
        return "Show all commands.";
    }

    @Override
    public final void execute() {
        System.out.println("[COMMANDS LIST]");
        System.out.println("DEFAULT USERS: user1:user1, admin:admin;");
        terminalService.printCommands();
    }
}
