package ru.kozyrev.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.kozyrev.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    public AboutCommand() {
        secure = true;
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Print information about build";
    }

    @Override
    public void execute() {
        final String developer = Manifests.read("developer");
        final String version = Manifests.read("version");
        System.out.printf("Developer: %s\nBuildNumber: %s\n", developer, version);
    }
}
