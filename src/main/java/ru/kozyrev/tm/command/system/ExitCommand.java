package ru.kozyrev.tm.command.system;

import ru.kozyrev.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    public ExitCommand() {
        secure = true;
    }

    @Override
    public final String getName() {
        return "exit";
    }

    @Override
    public final String getDescription() {
        return "Quit from manager.";
    }

    @Override
    public final void execute() {
        terminalService.closeSc();
        System.exit(0);
    }
}
