package ru.kozyrev.tm.command;

import ru.kozyrev.tm.api.service.ITerminalService;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.enumerated.RoleType;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;
    protected ITerminalService terminalService;
    protected Boolean secure;
    private List<RoleType> roleTypes = new ArrayList<>();

    public final void init(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.terminalService = serviceLocator.getTerminalService();
    }

    public AbstractCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        this.secure = false;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public final Boolean isSecure() {
        return secure;
    }

    public final List<RoleType> getRoleTypes() {
        return roleTypes;
    }
}
