package ru.kozyrev.tm.entity;

import java.util.UUID;

public abstract class AbstractEntity {
    private String id;
    private int shortLink;

    public AbstractEntity() {
        this.id = UUID.randomUUID().toString();
    }

    public abstract String getUserId();

    public final String getId() {
        return id;
    }

    public final void setId(final String id) {
        this.id = id;
    }

    public final int getShortLink() {
        return shortLink;
    }

    public final void setShortLink(final int shortLink) {
        this.shortLink = shortLink;
    }
}


