package ru.kozyrev.tm.entity;

import ru.kozyrev.tm.api.util.IEntityUtil;
import ru.kozyrev.tm.util.DateUtil;

import java.util.Date;

public final class Project extends AbstractEntity implements IEntityUtil {
    private String name = "";
    private String description = "";
    private Date dateStart;
    private Date dateFinish;
    private String userId;

    public Project() {
    }

    public Project(String name, String description, String dateStart, String dateFinish) throws Exception {
        this.name = name;
        this.description = description;
        this.dateStart = DateUtil.parseDate(dateStart);
        this.dateFinish = DateUtil.parseDate(dateFinish);
    }

    @Override
    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    @Override
    public final String getDescription() {
        return description;
    }

    public final void setDescription(final String description) {
        this.description = description;
    }

    public final Date getDateStart() {
        return dateStart;
    }

    public final void setDateStart(final Date dateStart) {
        this.dateStart = dateStart;
    }

    @Override
    public final String getDateStartAsStr() {
        return DateUtil.getDate(dateStart);
    }

    public final Date getDateFinish() {
        return dateFinish;
    }

    public final void setDateFinish(final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public final String getDateFinishAsStr() {
        return DateUtil.getDate(dateFinish);
    }

    @Override
    public final String getUserId() {
        return userId;
    }

    public final void setUserId(final String userId) {
        this.userId = userId;
    }
}
