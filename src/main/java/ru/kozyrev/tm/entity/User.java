package ru.kozyrev.tm.entity;

import ru.kozyrev.tm.enumerated.RoleType;

public final class User extends AbstractEntity {
    private String login = "";
    private String passwordHash = "";
    private RoleType roleType = RoleType.USER;

    public final String getLogin() {
        return login;
    }

    public final void setLogin(final String login) {
        this.login = login;
    }

    public final String getPasswordHash() {
        return passwordHash;
    }

    public final void setPasswordHash(final String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public final RoleType getRoleType() {
        return roleType;
    }

    public final void setRoleType(final RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public final String getUserId() {
        return getId();
    }
}
