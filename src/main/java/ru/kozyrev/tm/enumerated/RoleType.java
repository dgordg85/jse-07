package ru.kozyrev.tm.enumerated;

public enum RoleType {
    ADMIN("Администратор"),
    USER("Обычный пользователь");

    private final String displayName;

    RoleType(String displayName) {
        this.displayName = displayName;
    }

    public final String getDisplayName() {
        return displayName;
    }
}
