package ru.kozyrev.tm.exception.user;

public final class UserNoRoleException extends Exception {
    public UserNoRoleException() {
        super("No role for user!");
    }
}
