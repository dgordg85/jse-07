package ru.kozyrev.tm.exception.user;

public final class UserLoginChangeFailException extends Exception {
    public UserLoginChangeFailException() {
        super("Can't change login. Try again");
    }
}
