package ru.kozyrev.tm.util;

import ru.kozyrev.tm.exception.entity.IndexException;

public final class StringUtil {

    public final static int parseToInt(final String num) throws IndexException {
        try {
            return Integer.parseInt(num);
        } catch (NumberFormatException e) {
            throw new IndexException();
        }
    }
}
