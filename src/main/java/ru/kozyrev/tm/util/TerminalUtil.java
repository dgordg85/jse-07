package ru.kozyrev.tm.util;

import ru.kozyrev.tm.api.service.IProjectService;
import ru.kozyrev.tm.api.service.ITaskService;
import ru.kozyrev.tm.api.util.IEntityUtil;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.entity.IndexException;

import java.util.List;

public final class TerminalUtil {
    public final static void printAllTasks(final List<Task> taskList) {
        System.out.println("[TASKS LIST]");
        printList(taskList);
    }

    public final static void printTasksOfProject(final List<Task> taskList) {
        System.out.println("[TASKS LIST OF PROJECT]");
        printList(taskList);
    }

    public final static void printProjects(final List<Project> projectList) {
        System.out.println("[PROJECTS LIST]");
        printList(projectList);
    }

    public final static <T extends IEntityUtil> void printList(final List<T> list) {
        int count = 1;
        for (T entity : list) {
            System.out.printf("%d. %s, EDIT ID#%d, %s, s:%s, f:%s\n",
                    count++,
                    entity.getName(),
                    entity.getShortLink(),
                    entity.getDescription(),
                    entity.getDateStartAsStr(),
                    entity.getDateFinishAsStr()
            );
        }
    }

    public final static void clearProject(final IProjectService projectService, final ITaskService taskService, final String projectId, final String userId) throws Exception {
        clearTasks(taskService, projectId, userId);
        if (projectService.remove(projectId, userId) == null) {
            throw new IndexException();
        }
        System.out.println("[PROJECT REMOVED]");
        printProjects(projectService.findAll(userId));
    }

    public final static void clearTasks(final ITaskService taskService, final String projectId, final String userId) throws Exception {
        if (taskService.removeProjectTasks(projectId, userId)) {
            System.out.println("[ALL TASKS OF PROJECT REMOVED]");
        } else {
            System.out.println("[PROJECT HAVE NO TASKS]");
        }
    }

    public final static void clearAllTasks(final ITaskService taskService, final String userId) throws Exception {
        taskService.removeAll(userId);
        System.out.println("[ALL TASKS REMOVED]");
    }
}
