package ru.kozyrev.tm.util;

import ru.kozyrev.tm.exception.entity.DateException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {
    private static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    public final static String getDate(final Date date) {
        return formatter.format(date);
    }

    public final static Date parseDate(final String date) throws Exception {
        try {
            if (date == null || date.isEmpty()) {
                throw new DateException();
            }
            return formatter.parse(date);
        } catch (DateException | ParseException e) {
            throw e;
        }
    }
}
