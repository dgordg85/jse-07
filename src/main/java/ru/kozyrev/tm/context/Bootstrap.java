package ru.kozyrev.tm.context;

import ru.kozyrev.tm.api.service.*;
import ru.kozyrev.tm.exception.entity.DateException;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.repository.TaskRepository;
import ru.kozyrev.tm.repository.UserRepository;
import ru.kozyrev.tm.service.ProjectService;
import ru.kozyrev.tm.service.TaskService;
import ru.kozyrev.tm.service.TerminalService;
import ru.kozyrev.tm.service.UserService;
import ru.kozyrev.tm.util.EntityUtil;

import java.text.ParseException;

public final class Bootstrap implements ServiceLocator {
    private final IProjectService projectService = new ProjectService(new ProjectRepository());
    private final ITaskService taskService = new TaskService(new TaskRepository());
    private final IUserService userService = new UserService(new UserRepository());
    private final ITerminalService terminalService = new TerminalService(this);

    public final void init() throws Exception {
        getTerminalService().initCommands();
        EntityUtil.createDefaultUsers(getUserService());
        getTerminalService().welcomeMsg();
        String command;
        while (true) {
            try {
                command = terminalService.nextLine().toLowerCase().replace('_', '-');
                getTerminalService().execute(command);
            } catch (ParseException | DateException e) {
                getTerminalService().errorDateMsg();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    @Override
    public final IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public final ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public final IUserService getUserService() {
        return userService;
    }

    @Override
    public final ITerminalService getTerminalService() {
        return terminalService;
    }
}
