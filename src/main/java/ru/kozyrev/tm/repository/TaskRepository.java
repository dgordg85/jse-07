package ru.kozyrev.tm.repository;

import ru.kozyrev.tm.api.repository.ITaskRepository;
import ru.kozyrev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public final List<Task> findAll(final String projectId, final String userId) {
        List<Task> result = new ArrayList<>();
        List<Task> taskList = findAll(userId);
        for (Task task : taskList) {
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) {
                result.add(task);
            }
        }
        return result;
    }

    @Override
    public final boolean removeProjectTasks(final String projectId, final String userId) {
        boolean isDelete = false;
        List<Task> list = findAll(userId);
        for (Task task : list) {
            if (task.getProjectId().equals(projectId)) {
                remove(task.getId());
                isDelete = true;
            }
        }
        return isDelete;
    }
}