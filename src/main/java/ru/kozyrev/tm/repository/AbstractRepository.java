package ru.kozyrev.tm.repository;

import ru.kozyrev.tm.api.repository.IRepository;
import ru.kozyrev.tm.entity.AbstractEntity;
import ru.kozyrev.tm.exception.entity.EntityException;
import ru.kozyrev.tm.exception.entity.IndexException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {
    private final Map<String, T> map = new LinkedHashMap<>();
    private int count = 1;

    @Override
    public final T findOne(final String id) {
        return map.get(id);
    }

    @Override
    public final T persist(final T entity) throws Exception {
        if (map.containsKey(entity.getId())) {
            throw new EntityException();
        }
        map.put(entity.getId(), entity);
        entity.setShortLink(count++);
        return entity;
    }

    @Override
    public final T merge(final T entity) {
        map.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public final T remove(final String id) {
        return map.remove(id);
    }

    @Override
    public final void removeAll() {
        map.clear();
    }

    @Override
    public final List<T> findAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    public final List<T> findAll(final String userId) {
        List<T> list = new ArrayList<>();
        List<T> allEntities = findAll();
        for (T entity : allEntities) {
            if (entity.getUserId().equals(userId)) {
                list.add(entity);
            }
        }
        return list;
    }

    @Override
    public final T findOne(final String id, final String userId) {
        T entity = findOne(id);
        return entity.getUserId().equals(userId) ? entity : null;
    }

    @Override
    public final T merge(final T entity, final String userId) {
        T entityUpdate = findOne(entity.getId());
        if (entityUpdate.getUserId().equals(userId)) {
            merge(entity);
            return entity;
        }
        return null;
    }

    @Override
    public final T remove(final String id, final String userId) {
        T entity = findOne(id);
        if (entity.getUserId().equals(userId)) {
            return remove(id);
        }
        return null;
    }

    @Override
    public final void removeAll(final String userId) {
        List<T> allEntities = findAll();
        for (T entity : allEntities) {
            if (entity.getUserId().equals(userId)) {
                remove(entity.getId());
            }
        }
    }

    @Override
    public final T findOne(final int shortLink, final String userId) throws Exception {
        List<T> allEntities = findAll();

        for (T entity : allEntities) {
            if (entity.getShortLink() == shortLink) {
                if (entity.getUserId().equals(userId)) {
                    return entity;
                } else {
                    throw new IndexException();
                }
            }
        }
        return null;
    }
}
