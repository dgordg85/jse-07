package ru.kozyrev.tm.repository;

import ru.kozyrev.tm.api.repository.IUserRepository;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public final User getUserByLogin(final String login) {
        List<User> list = findAll();
        for (User user : list) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        return null;
    }
}
