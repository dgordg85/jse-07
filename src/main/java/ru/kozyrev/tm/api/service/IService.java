package ru.kozyrev.tm.api.service;

import java.util.List;

public interface IService<T> {
    List<T> findAll();

    T findOne(String id, String userId) throws Exception;

    List<T> findAll(String userId) throws Exception;

    T remove(String entityId, String userId) throws Exception;

    void removeAll(String userId) throws Exception;

    T getEntityByNum(String num, String userId) throws Exception;

    String getEntityIdByNum(String num, String userId) throws Exception;
}
