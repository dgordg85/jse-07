package ru.kozyrev.tm.api.service;

import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IUserService {
    List<User> findAll();

    User findOne(String id, String userId) throws Exception;

    List<User> findAll(String userId) throws Exception;

    User remove(String entityId, String userId) throws Exception;

    void removeAll(String userId) throws Exception;

    User getEntityByNum(String num, String userId) throws Exception;

    String getEntityIdByNum(String num, String userId) throws Exception;

    User persist(User user) throws Exception;

    User merge(User user) throws Exception;

    User remove(String id);

    User getUserByLogin(String login);

    boolean isPasswordNotTrue(String userId, String password) throws Exception;
}
