package ru.kozyrev.tm.api.service;

import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.CommandCorruptException;
import ru.kozyrev.tm.exception.command.CommandException;

import java.util.Scanner;

public interface ITerminalService {
    void printCommands();

    void registryCommand(final AbstractCommand command) throws CommandCorruptException;

    void execute(final String commandStr) throws Exception;

    boolean isCommandNotAllow(final AbstractCommand command) throws CommandException;

    void initCommands() throws CommandCorruptException;

    void welcomeMsg();

    void errorDateMsg();

    String nextLine();

    User getCurrentUser();

    void setCurrentUser(final User user);

    void closeSc();

    boolean isUserAuth();

    void setSc(final Scanner sc);

    String getCurrentUserId();
}
