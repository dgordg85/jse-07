package ru.kozyrev.tm.api.service;

import ru.kozyrev.tm.entity.Project;

import java.util.List;

public interface IProjectService {
    List<Project> findAll();

    Project findOne(String id, String userId) throws Exception;

    List<Project> findAll(String userId) throws Exception;

    Project remove(String entityId, String userId) throws Exception;

    void removeAll(String userId) throws Exception;

    Project getEntityByNum(String num, String userId) throws Exception;

    String getEntityIdByNum(String num, String userId) throws Exception;

    Project persist(Project project, String userId) throws Exception;

    Project merge(Project project, String userId) throws Exception;
}
