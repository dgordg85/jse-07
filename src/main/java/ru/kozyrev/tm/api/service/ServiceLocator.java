package ru.kozyrev.tm.api.service;

public interface ServiceLocator {
    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    ITerminalService getTerminalService();
}
