package ru.kozyrev.tm.api.service;

import ru.kozyrev.tm.entity.Task;

import java.util.List;

public interface ITaskService {
    List<Task> findAll();

    Task findOne(String id, String userId) throws Exception;

    List<Task> findAll(String userId) throws Exception;

    Task remove(String entityId, String userId) throws Exception;

    void removeAll(String userId) throws Exception;

    Task getEntityByNum(String num, String userId) throws Exception;

    String getEntityIdByNum(String num, String userId) throws Exception;

    List<Task> findAll(String projectId, String userId) throws Exception;

    Task persist(Task task, String userId) throws Exception;

    Task merge(Task task, String userId) throws Exception;

    boolean removeProjectTasks(String projectId, String userId) throws Exception;
}
