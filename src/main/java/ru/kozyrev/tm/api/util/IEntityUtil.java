package ru.kozyrev.tm.api.util;

public interface IEntityUtil {
    String getName();

    String getDescription();

    String getDateStartAsStr();

    String getDateFinishAsStr();

    int getShortLink();
}
