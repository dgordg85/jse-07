package ru.kozyrev.tm.api.repository;

import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IUserRepository {
    User findOne(String id);

    User persist(User entity) throws Exception;

    User merge(User entity);

    User remove(String id);

    void removeAll();

    List<User> findAll();

    List<User> findAll(String userId);

    User findOne(String id, String userId);

    User merge(User entity, String userId);

    User remove(String id, String userId);

    void removeAll(String userId);

    User findOne(int shortLink, String userId) throws Exception;

    User getUserByLogin(String login);
}
