package ru.kozyrev.tm.api.repository;

import java.util.List;

public interface IRepository<T> {
    T findOne(String id);

    T persist(T entity) throws Exception;

    T merge(T entity);

    T remove(String id);

    void removeAll();

    List<T> findAll();

    List<T> findAll(String userId);

    T findOne(String id, String userId);

    T merge(T entity, String userId);

    T remove(String id, String userId);

    void removeAll(String userId);

    T findOne(int shortLink, String userId) throws Exception;
}
