package ru.kozyrev.tm.api.repository;

import ru.kozyrev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {
    Task findOne(String id);

    Task persist(Task entity) throws Exception;

    Task merge(Task entity);

    Task remove(String id);

    void removeAll();

    List<Task> findAll();

    List<Task> findAll(String userId);

    Task findOne(String id, String userId);

    Task merge(Task entity, String userId);

    Task remove(String id, String userId);

    void removeAll(String userId);

    Task findOne(int shortLink, String userId) throws Exception;

    List<Task> findAll(String projectId, String userId);

    boolean removeProjectTasks(String projectId, String userId);
}
