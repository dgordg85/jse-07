package ru.kozyrev.tm.api.repository;

import ru.kozyrev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {
    Project findOne(String id);

    Project persist(Project entity) throws Exception;

    Project merge(Project entity);

    Project remove(String id);

    void removeAll();

    List<Project> findAll();

    List<Project> findAll(String userId);

    Project findOne(String id, String userId);

    Project merge(Project entity, String userId);

    Project remove(String id, String userId);

    void removeAll(String userId);

    Project findOne(int shortLink, String userId) throws Exception;
}
