package ru.kozyrev.tm.service;

import ru.kozyrev.tm.api.repository.IProjectRepository;
import ru.kozyrev.tm.api.service.IProjectService;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.exception.entity.DateException;
import ru.kozyrev.tm.exception.entity.DescriptionException;
import ru.kozyrev.tm.exception.entity.EntityOwnerException;
import ru.kozyrev.tm.exception.entity.NameException;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.repository.ProjectRepository;

public final class ProjectService extends AbstractService<Project> implements IProjectService {
    private final IProjectRepository projectRepository = (ProjectRepository) abstractRepository;

    public ProjectService(ProjectRepository projectRepository) {
        super(projectRepository);
    }

    @Override
    public final Project persist(final Project project, final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (project.getName() == null || project.getName().isEmpty()) {
            throw new NameException();
        }
        if (project.getUserId() == null || project.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (project.getDescription() == null || project.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        return projectRepository.persist(project);
    }

    @Override
    public final Project merge(final Project project, final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (project.getName() == null) {
            throw new NameException();
        }
        if (project.getUserId() == null || project.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (project.getDescription() == null) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        Project projectUpdate = findOne(project.getId(), userId);
        if (!project.getName().isEmpty()) {
            projectUpdate.setName(project.getName());
        }
        if (!project.getDescription().isEmpty()) {
            projectUpdate.setDescription(project.getDescription());
        }
        projectUpdate.setDateStart(project.getDateStart());
        projectUpdate.setDateFinish(project.getDateFinish());
        return projectRepository.merge(projectUpdate, userId);
    }
}
