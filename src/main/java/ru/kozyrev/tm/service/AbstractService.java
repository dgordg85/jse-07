package ru.kozyrev.tm.service;

import ru.kozyrev.tm.api.service.IService;
import ru.kozyrev.tm.entity.AbstractEntity;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.repository.AbstractRepository;
import ru.kozyrev.tm.util.StringUtil;

import java.util.List;

public class AbstractService<T extends AbstractEntity> implements IService<T> {
    AbstractRepository<T> abstractRepository;

    public AbstractService(AbstractRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Override
    public final List<T> findAll() {
        return abstractRepository.findAll();
    }

    @Override
    public final T findOne(final String id, final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (id == null || id.isEmpty()) {
            return null;
        }
        return abstractRepository.findOne(id);
    }

    @Override
    public final List<T> findAll(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        return abstractRepository.findAll(userId);
    }

    @Override
    public final T remove(final String entityId, final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (entityId == null || entityId.isEmpty()) {
            return null;
        }
        return abstractRepository.remove(entityId, userId);
    }

    @Override
    public final void removeAll(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        abstractRepository.removeAll(userId);
    }

    @Override
    public final T getEntityByNum(final String num, final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }
        int shortLink = StringUtil.parseToInt(num);
        T entity = abstractRepository.findOne(shortLink, userId);

        if (entity == null) {
            throw new IndexException();
        }
        return entity;
    }

    @Override
    public final String getEntityIdByNum(final String num, final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }
        return getEntityByNum(num, userId).getId();
    }
}
