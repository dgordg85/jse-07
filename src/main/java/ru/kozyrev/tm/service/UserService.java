package ru.kozyrev.tm.service;

import ru.kozyrev.tm.api.repository.IUserRepository;
import ru.kozyrev.tm.api.service.IUserService;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.user.*;
import ru.kozyrev.tm.repository.UserRepository;
import ru.kozyrev.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {
    private final IUserRepository userRepository = (UserRepository) abstractRepository;

    public UserService(UserRepository userRepository) {
        super(userRepository);
    }

    @Override
    public final User persist(final User user) throws Exception {
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (getUserByLogin(user.getLogin()) != null) {
            throw new UserLoginTakenException();
        }
        if (user.getPasswordHash() == null || user.getPasswordHash().isEmpty() || user.getPasswordHash().equals(HashUtil.EMPTY_PASSWORD)) {
            throw new UserPasswordEmptyException();
        }
        return userRepository.persist(user);
    }

    @Override
    public final User merge(final User user) throws Exception {
        if (user.getLogin() == null) {
            throw new UserLoginEmptyException();
        }
        if (user.getPasswordHash() == null) {
            throw new UserPasswordEmptyException();
        }
        if (user.getRoleType() == null) {
            throw new UserNoRoleException();
        }
        User userUpdate = userRepository.findOne(user.getId());

        if (!user.getLogin().isEmpty()) {
            userUpdate.setLogin(user.getLogin());
        }

        if (user.getPasswordHash().isEmpty() || user.getPasswordHash().equals(HashUtil.EMPTY_PASSWORD)) {
            throw new UserPasswordEmptyException();
        }
        userUpdate.setPasswordHash(user.getPasswordHash());
        userUpdate.setRoleType(user.getRoleType());

        return userRepository.merge(userUpdate);
    }

    @Override
    public final User remove(final String id) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        return userRepository.remove(id);
    }

    @Override
    public final User getUserByLogin(final String login) {
        if (login == null || login.isEmpty()) {
            return null;
        }
        return userRepository.getUserByLogin(login);
    }

    @Override
    public final boolean isPasswordNotTrue(final String userId, final String password) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (password == null || password.isEmpty()) {
            throw new UserPasswordEmptyException();
        }
        return !userRepository.findOne(userId).getPasswordHash().equals(HashUtil.getHash(password));
    }
}
