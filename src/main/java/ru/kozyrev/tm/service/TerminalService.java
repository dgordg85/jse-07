package ru.kozyrev.tm.service;

import ru.kozyrev.tm.api.service.ITerminalService;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.command.project.*;
import ru.kozyrev.tm.command.system.AboutCommand;
import ru.kozyrev.tm.command.system.ExitCommand;
import ru.kozyrev.tm.command.system.HelpCommand;
import ru.kozyrev.tm.command.task.*;
import ru.kozyrev.tm.command.user.*;
import ru.kozyrev.tm.context.Bootstrap;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.AccessForbiddenException;
import ru.kozyrev.tm.exception.command.CommandCorruptException;
import ru.kozyrev.tm.exception.command.CommandException;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class TerminalService implements ITerminalService {
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final Bootstrap bootstrap;
    private Scanner sc = new Scanner(System.in);
    private User currentUser;

    public TerminalService(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public final void printCommands() {
        for (AbstractCommand command : commands.values()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }

    @Override
    public final void registryCommand(final AbstractCommand command) throws CommandCorruptException {
        if (command.getName() == null || command.getName().isEmpty())
            throw new CommandCorruptException();
        if (command.getDescription() == null || command.getDescription().isEmpty())
            throw new CommandCorruptException();
        command.init(bootstrap);
        commands.put(command.getName(), command);
    }

    @Override
    public final void execute(final String commandStr) throws Exception {
        if (commandStr == null || commandStr.isEmpty()) {
            throw new CommandException();
        }
        AbstractCommand command = commands.get(commandStr);
        if (isCommandNotAllow(command)) {
            throw new AccessForbiddenException();
        }
        command.execute();
    }

    @Override
    public final boolean isCommandNotAllow(final AbstractCommand command) throws CommandException {
        if (command == null) {
            throw new CommandException();
        }
        boolean isSecureCheck = command.isSecure() || isUserAuth();

        boolean isRoleAllowed;
        if (currentUser != null) {
            isRoleAllowed = command.getRoleTypes().contains(currentUser.getRoleType());
        } else
            isRoleAllowed = true;

        if (!isSecureCheck || !isRoleAllowed) {
            return true;
        }
        return false;
    }

    @Override
    public final void initCommands() throws CommandCorruptException {
        registryCommand(new UserCreateCommand());
        registryCommand(new UserLoginCommand());
        registryCommand(new UserLogoutCommand());
        registryCommand(new UserProfileCommand());
        registryCommand(new UserUpdateCommand());
        registryCommand(new UserRemoveCommand());
        registryCommand(new UserPasswordUpdateCommand());
        registryCommand(new ProjectListCommand());
        registryCommand(new ProjectCreateCommand());
        registryCommand(new ProjectUpdateCommand());
        registryCommand(new ProjectRemoveCommand());
        registryCommand(new ProjectClearCommand());
        registryCommand(new TaskListCommand());
        registryCommand(new TaskClearCommand());
        registryCommand(new TaskCreateCommand());
        registryCommand(new TaskUpdateCommand());
        registryCommand(new TaskRemoveCommand());
        registryCommand(new HelpCommand());
        registryCommand(new ExitCommand());
        registryCommand(new AboutCommand());
    }

    @Override
    public final void welcomeMsg() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    @Override
    public final void errorDateMsg() {
        System.out.println("Wrong date! User dd-MM-YYYY format");
    }

    @Override
    public final String nextLine() {
        return sc.nextLine();
    }

    @Override
    public final User getCurrentUser() {
        return currentUser;
    }

    @Override
    public final void setCurrentUser(final User user) {
        currentUser = user;
    }

    @Override
    public final void closeSc() {
        sc.close();
    }

    @Override
    public final boolean isUserAuth() {
        return currentUser != null;
    }

    @Override
    public final void setSc(final Scanner sc) {
        this.sc = sc;
    }

    @Override
    public final String getCurrentUserId() {
        return currentUser.getId();
    }
}
