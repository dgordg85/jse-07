package ru.kozyrev.tm.service;

import ru.kozyrev.tm.api.repository.ITaskRepository;
import ru.kozyrev.tm.api.service.ITaskService;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.exception.entity.*;
import ru.kozyrev.tm.exception.user.UserEmptyException;
import ru.kozyrev.tm.repository.TaskRepository;

import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {
    private final ITaskRepository taskRepository = (TaskRepository) abstractRepository;

    public TaskService(TaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    public final List<Task> findAll(final String projectId, final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        return taskRepository.findAll(projectId, userId);
    }

    @Override
    public final Task persist(final Task task, final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (task.getName() == null || task.getName().isEmpty()) {
            throw new NameException();
        }
        if (task.getUserId() == null || task.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
            throw new IndexException();
        }
        if (task.getDescription() == null || task.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (task.getDateStart() == null) {
            throw new DateException();
        }
        if (task.getDateFinish() == null) {
            throw new DateException();
        }
        return taskRepository.persist(task);
    }

    @Override
    public final Task merge(final Task task, final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (task.getName() == null) {
            throw new NameException();
        }
        if (task.getUserId() == null || task.getUserId().isEmpty()) {
            throw new EntityOwnerException();
        }
        if (task.getProjectId() == null) {
            throw new IndexException();
        }
        if (task.getDescription() == null) {
            throw new DescriptionException();
        }

        Task taskUpdate = findOne(task.getId(), userId);
        if (taskUpdate == null) {
            persist(task, userId);
        }

        if (!task.getName().isEmpty()) {
            taskUpdate.setName(task.getName());
        }
        if (!task.getProjectId().isEmpty()) {
            taskUpdate.setProjectId(task.getProjectId());
        }
        if (!task.getDescription().isEmpty()) {
            taskUpdate.setDescription(task.getDescription());
        }
        if (task.getDateStart() != null) {
            taskUpdate.setDateStart(task.getDateStart());
        }
        if (task.getDateFinish() != null) {
            taskUpdate.setDateFinish(task.getDateFinish());
        }
        return taskRepository.merge(taskUpdate, userId);
    }

    @Override
    public final boolean removeProjectTasks(final String projectId, final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        return taskRepository.removeProjectTasks(projectId, userId);
    }
}
